function tinh(){
    var a=document.getElementById("chieu-dai").value*1
    var b=document.getElementById("chieu-rong").value*1
    var dienTich=a*b
    var chuVi=(a+b)*2
    document.getElementById("tinh").innerText=`Diện tích là: ` + dienTich + "  ;  " + "Chu vi là: "+ chuVi;
}
// - input:chiều dài và chiều rộng hình chữ nhật
// - các bước xử lí: 
// b1: tạo biến a,b tương ứng với chiều dài và chiều rộng;tạo biến dienTich ứng với diện tích,tạo biến chuVi ứng với chu vi . 
// b2: gán giá trị cho a,b    
// b3: áp dụng công thức tính dienTich=a*b và chuVi=(a+b)*2
// b4 :in kết quả diện tích và chu vi ra màn hình
// - output: giá trị diện tích và chu vi        